@extends('dashboard.template.main')
@section('content')
<div class="container mt-4">
    <div class="row">
        <div class="col-md-12">
            <div class='card'>
                <div class="card-header primary" style='background: #00578d'>
                    Create new Loans
                </div>
                <div class='card-body'>
                    <add-loanz></add-loanz>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection