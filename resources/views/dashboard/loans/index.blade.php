@extends('dashboard.template.main')
@section('content')
<div class="container mt-4">
    <div class="row">
        <div class="col-md-12">
            <div class='card'>
                <div class="card-header primary" style='background: #00578d'>
                    Loans
                    <a style="float:right" href="{{url('dashboard/loans/add')}}" class='btn btn-primary'><i class="fa fa-plus"></i></a>
                </div>
                <div class='card-body'>
                <table id="example" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>Reference</th>
                            <th>Loan amont</th>
                            <th>Annual interest rate</th>
                            <th>First payment date</th>
                            <th>Payment frequency</th>
                            <th>Total Payment</th>
                            <th>Total insterest</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($loans as $key => $loan)
                            <tr>
                                <td>{{$loan->id}}</td>
                                <td>{{$loan->loans_amount}}</td>
                                <td>{{$loan->annual_interest_rate}}</td>
                                <td>{{$loan->first_payment}}</td>
                                <td>{{$loan->payment_frequency}}</td>
                                <td>{{$loan->total_payment}}</td>
                                <td>{{$loan->total_interest}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection