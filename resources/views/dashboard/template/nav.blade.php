<section class="menu header_section">
    <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm bg-color">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <div class="hamburger">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </button>
        <div class="container" style="max-width: 1100px">
            <div class="menu-logo">
                <a href="/">
                    <div class="navbar-brand">
                        <img src="{{url('assets/images/logounifam.svg')}}" width="200px" height="70px" alt="Guimeco Logo" title="Guimeco, la mejor guia médica colombiana">
                    </div>
                </a>
            </div>
            <ul class="navbar-nav ml-auto mt-4">
                <li class="nav-item mt-1 hide-mobile" style="max-width: 500px">
                    <div class="form_input_group row">
                        <div class="form_icon col-1"><i class="fa fa-search" style="font-size: 14px"></i></div>
                        <div class="form_input col-11"><input type="text" name="search" placeholder="search" /></div>
                    </div>
                </li>
                <li class="nav-item dropdown mt-1">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        <i class="fa fa-envelope"></i>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>
                    </div>
                </li>
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            @if(Auth::user()->profile_photo)
                            <img src="{{url('assets/images/profile/', Auth::user()->profile_photo)}}" width="30px" height="30px" />
                                @else
                                <img src="{{url('assets/images/profilenone.png')}}" width="30px" height="30px" />
                            @endif
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
            </ul>
        </div>
    </nav>
</section>
<div class='container' style='max-width: 1120px'>
        <section class="header_menu">
            <div class="row">
                <div style="padding: 6px; padding-left: 25px">
                    <a href="{{url('/dashboard/loans')}}">
                        <img src="{{url('assets/images/icon2.png')}}" width="20px" height="20px" /> 
                        <span>Loans</span>
                    </a>
                </div>
                <div style="padding: 6px; padding-left: 25px">
                    <a href="{{url('/dashboard/savings')}}">
                        <img src="{{url('assets/images/icon3.png')}}" width="20px" height="20px" /> 
                        <span>Savings</span>
                    </a>
                </div>
                <div style="padding: 6px; padding-left: 25px">
                    <a href="{{url('/dashboard/investments')}}">
                        <img src="{{url('assets/images/icon4.png')}}" width="20px" height="20px" /> 
                        <span>Investments</span>
                    </a>
                </div>
                <div style="padding: 6px; padding-left: 25px">
                    <a href="{{url('/dashboard/loan')}}">
                        <img src="{{url('assets/images/icon5.png')}}" width="20px" height="20px" /> 
                        <span>Rent To Own</span>
                    </a>
                </div>
            </div>
        </section>
        <section class="header_title">
            <h4 class="text-white">Dashboard</h4>
        </section>
    </div>