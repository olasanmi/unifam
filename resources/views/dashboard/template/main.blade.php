<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>UNIFAM</title>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('assets/css/main.css')}}">
    <link rel="stylesheet" type="text/css" href="<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
    <script src="https://use.fontawesome.com/f17efb1d96.js"></script>
    @toastr_css
</head>
<body style="background: white !important">
    <div id="app">
        @include('dashboard.template.nav')
        @include('dashboard.template.sidebar')
        <div id="main">
            @yield('content')
        </div>
    </div>
    <!-- <script src="{{ url('assets/js/main.js') }}" ></script> -->
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="{{asset('js/app.js')}}" ></script>
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script src='https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/js/all.min.js"></script>
    @toastr_js
    @toastr_render
    <script>
        var mini = true;
        function toggleSidebar() {
            if (mini) {
                document.getElementById("mySidebar").style.width = "200px";
                document.getElementById("main").style.marginLeft = "200px";
                this.mini = false;
            } else {
                document.getElementById("mySidebar").style.width = "85px";
                document.getElementById("main").style.marginLeft = "85px";
                this.mini = true;
            }
        }
    </script>
</body>
</html>
