@extends('dashboard.template.main')
@section('content')
<div class="container">
    <section class="header_panel row">
        <div class="col-12 col-md-3 col-lg-3 mt-1 p-1" style="padding: 0">
            <div class="card p-2 header_panel_card">
                <div class="row">
                    <div class="col-md-4 col-3 col-sm-5 mt-2">
                        <img src="{{url('assets/images/icon6.png')}}" width="50px" height="50px" />
                    </div>
                    <div class="col-md-8 col-9 col-sm-7 mt-2 row">
                        <div class="number" style="width: 100%">87</div><br>
                        <div class="caption">Current active jobs</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-3 col-lg-3 mt-1 p-1" style="padding: 0">
            <div class="card p-2 header_panel_card">
                <div class="row">
                    <div class="col-md-4 col-3 col-sm-5 mt-2">
                        <img src="{{url('assets/images/icon7.png')}}" width="50px" height="50px" />
                    </div>
                    <div class="col-md-8 col-9 col-sm-7 mt-2 row">
                        <div class="number" style="width: 100%">5</div><br>
                        <div class="caption">Messages</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-3 col-lg-3 mt-1 p-1" style="padding: 0">
            <div class="card p-2 header_panel_card">
                <div class="row">
                    <div class="col-md-4 col-3 col-sm-5 mt-2">
                        <img src="{{url('assets/images/icon8.png')}}" width="50px" height="50px" />
                    </div>
                    <div class="col-md-8 col-9 col-sm-7 mt-2 row">
                        <div class="number" style="width: 100%">11</div><br>
                        <div class="caption">Pending Tasks</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-3 col-lg-3 mt-1 p-1" style="padding: 0">
            <div class="card p-2 header_panel_card">
                <div class="row">
                    <div class="col-md-4 col-3 col-sm-5 mt-2">
                        <img src="{{url('assets/images/icon9.png')}}" width="50px" height="50px" />
                    </div>
                    <div class="col-md-8 col-9 col-sm-7 mt-2 row">
                        <div class="number" style="width: 100%">35</div><br>
                        <div class="caption">New files upload</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="metrics mt-4">
        <div class="row">
            <div class="col-12 col-md-6 mt-4 mb-2">
                <div class="card">
                    <div class="card-header">
                        <span>Loans</span><div class="icon_metrics"><a href="{{url('dashboard/add/loans')}}"><i class="fa fa-plus"></i></a></div>
                        <button class="btn">View all</button>
                    </div>
                    <div class="card-body" style="padding: 0">
                    <img src="{{url('assets/images/metrica1.png')}}" style="margin-top: -4px" width="100%" height="200px" />
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 mt-4 mb-2">
                <div class="card">
                    <div class="card-header">
                        <span>Savings</span><div class="icon_metrics" style="margin-left: 53px"><a href="{{url('dashboard/add/savings')}}"><i class="fa fa-plus"></i></a></div>
                        <button class="btn">View all</button>
                    </div>
                    <div class="card-body" style="padding: 0">
                    <img src="{{url('assets/images/metrica2.png')}}" style="margin-top: -4px" width="100%" height="200px" />
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 mt-4 mb-2">
                <div class="card">
                    <div class="card-header">
                        <span>Investments</span><div class="icon_metrics" style="margin-left: 78px"><a href="{{url('dashboard/add/loans')}}"><i class="fa fa-plus"></i></a></div>
                        <button class="btn">View all</button>
                    </div>
                    <div class="card-body" style="padding: 0">
                    <img src="{{url('assets/images/metrica3.png')}}" style="margin-top: -4px" width="100%" height="200px" />
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 mt-4 mb-2">
                <div class="card">
                    <div class="card-header">
                        <span>Rent To Own</span><div class="icon_metrics" style="margin-left: 84px"><a href="{{url('dashboard/add/loans')}}"><i class="fa fa-plus"></i></a></div>
                        <button class="btn">View all</button>
                    </div>
                    <div class="card-body" style="padding: 0">
                    <img src="{{url('assets/images/metrica1.png')}}" style="margin-top: -4px" width="100%" height="200px" />
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection