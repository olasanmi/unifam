<?php

use Illuminate\Database\Seeder;

class UserTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'juanpa2020',
            'email' => 'juanpa@gmail.com',
            'password' => bcrypt('juanpa2020')
        ]);
    }
}
