$(document).ready(function () {

    $('.collapse-border').on('click', function () {
      alert(123)
        $('#sidebar').toggleClass('active');
    });
 
});

$(document).ready(function() {
    $('#example').DataTable()
} )

$(document).ready(function() {
  $('#hide_sidebar').click(function() {
    $('.sidebar').toggle()
  })
} )

$(document).ready(function() {
  
  var loanYear = 1;
  var paymentCycle = 1;
  var monthlyRepayment = 0;
  var monthlyInterest = 0;
  var amortData = [];

  $(function() {
    //PTM
    function calculatePmt(i, n, p) {
        return i * p * Math.pow((1 + i), n) / (1 - Math.pow((1 + i), n));
    }
    // Way 1
    function calculateTotalLoans() {
      var type = $('#type').val()

      if (type === 'interest-Only') {
        calculateTotalLoansInterestOnly()
      }

      if (type === 'amortized') {
        calculateTotalLoansAmortizedOnly()
      }
    }
    //loans amortized
    function calculateTotalLoansAmortizedOnly() {
      var amont = $("#loans_amount").val()
      var period = $("#terms_in_year").val()
      var paymentFrequency = $("#payment_frequency").val()
      var totalPayment = 0
      if (paymentFrequency === 'montly') {
        totalPayment = period * 12
        totalPayment = totalPayment + 'm'
        paymentCycle = 1
      } else if (paymentFrequency === 'weekly') {
        totalPayment = period * 52
        totalPayment = totalPayment + 'w'
        paymentCycle = 2
      }
      calculateLoan(paymentCycle)
    }

    function getAmortData(dataType, terms){
    var dataValue = 0;
    switch(dataType){
      case "interest":
        for(var i = 0; i < terms; i++){
          dataValue += parseFloat(amortData[i].Interest);
        }
        break;
      case "balance":
        dataValue = parseFloat(amortData[terms-1].Balance);
        break;
    }
  }
  
  //calculate function
  function calculateLoan(ddata){
    var loanBorrow = parseFloat($("#loans_amount").val());
    var interestRate = parseFloat($("#annual_interest_rate").val()) / 1200;
    var totalTerms = 12 * $("#terms_in_year").val();

    //Monthly
    var schedulePayment = Math.round(loanBorrow * interestRate / (1 - (Math.pow(1/(1 + interestRate), totalTerms))));
    monthlyRepayment = schedulePayment;
    var totalPayment = totalTerms * schedulePayment;
    // amort(loanBorrow, parseFloat($("#annual_interest_rate").val())/100, totalTerms);
    switch(parseInt(ddata)){
      case 2:
        //Weekly
        //we multiple by 12 then divided by 52 
        schedulePayment = Math.round((schedulePayment * 12) / 52);
        break;
    }
    var ee = 12
    if (ddata == 2) {
      ee = 52
    }
    $("#total_payment").val(Math.round( (parseFloat(schedulePayment) * ee) * 100) / 100)
    $("#montly_payment").val(Math.round( ( parseFloat(schedulePayment) ) * 100) / 100)
    $("#total_interest").val(Math.round( ( (parseFloat(schedulePayment) * ee) - $("#loans_amount").val() ) * 100) / 100)
    amort(loanBorrow, parseFloat($("#annual_interest_rate").val())/100, totalTerms);
    calculateTotalPyment()
  }

  //function to calculate the amortization data
  function amort(balance, interestRate, terms)
  {
    amortData = [];
    //Calculate the per month interest rate
    var monthlyRate = interestRate/12;
    //Calculate the payment
    var payment = balance * (monthlyRate/(1-Math.pow(1+monthlyRate, -terms)));
    for (var count = 0; count < terms; ++count)
    { 
      var interest = balance * monthlyRate;
      var monthlyPrincipal = payment - interest;
      var amortInfo = {
        Balance: balance.toFixed(2),
        Interest: balance * monthlyRate,
        MonthlyPrincipal: monthlyPrincipal
      }
      amortData.push(amortInfo);
      balance = balance - monthlyPrincipal.toFixed(2); 
    }
  }

    // calculate total depend rate for loan amont
    function calculateTotalLoansInterestOnly() {
      var amont = $("#loans_amount").val()
      var rate = $("#rate_per_period").val() / 100
      var period = $("#terms_in_year").val()
      var paymentFrequency = $("#payment_frequency").val()
      var totalPayment = 0
      var a = 0
      if (paymentFrequency === 'montly') {
        totalPayment = period * 12
        a = 12
      } else if (paymentFrequency === 'weekly') {
        totalPayment = period * 52
        a = 52
      }
      
      var interestToPay = rate * amont
      var totalInterest = 0
      
      for (let i = 1; i <= totalPayment; i++) {
        if (i === totalPayment) {
          totalInterest = totalInterest + interestToPay 
        } else {
          totalInterest = totalInterest + interestToPay 
        }
      }
    
    var baseAndInterest = parseFloat(amont) + parseFloat(totalInterest)
      $("#total_interest").val(Math.round( totalInterest * 100) / 100)
      $("#total_payment").val(Math.round( baseAndInterest * 100) / 100)
      $("#montly_payment").val(Math.round( ( parseFloat(totalInterest) / a ) * 100) / 100)
      calculateTotalPyment()
    }

    // total payment
    function calculateTotalPyment() {
      var period = $("#terms_in_year").val()
      var paymentFrequency = $("#payment_frequency").val()
      var totalPayment = 0
      if (paymentFrequency === 'montly') {
        totalPayment = period * 12
      } else if (paymentFrequency === 'weekly') {
        totalPayment = period * 52
      }
      $('#numbers_payment').val(totalPayment)
    }

    // calculate total rate
    function calculateRate() {
      var type = $('#type').val()
      if (type === 'interest-Only' || type === 'amortized') {
        var interest = $("#annual_interest_rate").val() / 100
        var compoundPeriod = $("#compound_period").val()
        //set compountPeriod
        var compoundPeriodInInteger = 0
        if (compoundPeriod === 'montly') {
          compoundPeriodInInteger = 12
        } else if (compoundPeriod === 'weekly') {
          compoundPeriodInInteger = 52
        }
        // set payment frequency
        var paymentFrequency = $("#payment_frequency").val()
        var paymentFrequencyInInteger = 0
        if (paymentFrequency === 'montly') {
          paymentFrequencyInInteger = 12
        } else if (paymentFrequency === 'weekly') {
          paymentFrequencyInInteger = 52
        }
        var total  = Math.pow(( 1 + interest / compoundPeriodInInteger ), (compoundPeriodInInteger / paymentFrequencyInInteger)) - 1
        $("#rate_per_period").val(Math.round((total * 100) * 100) / 100 )

        calculateTotalLoans()
      }
    }

    function setTable(array) {
      console.log(array)
    }

    //calls function
    $('#annual_interest_rate').blur(function () {
      calculateRate()
    })

     $('#type').change(function () {
      calculateRate()
      if ($(this).val() == 'amortized') {
        $(".showCalculator").toggle()
        $(".showInterestOnline").toggle()
      }
      if ($(this).val() == 'interest-Only') {
        $(".showCalculator").toggle()
        $(".showInterestOnline").toggle()
      }
    })
    
    // calculate total payment per loans
    $('#terms_in_year').blur(function () {
      calculateRate()
    })
    
    //calculate payment total
    $('#payment_frequency').change(function () {
      calculateRate()
    })

    //calculate payment total
    $('#compound_period').change(function () {
      calculateRate()
    })

    $(document).ready(function(){
        $(".calculator").accrue({
  mode: "amortization"
});
    });

    var loan_info = $.loanInfo({
      amount: "$10000",
      rate: "18%",
      term: "104w"
    });

// log the calculation data for test purposes
console.log( loan_info );

  })
})