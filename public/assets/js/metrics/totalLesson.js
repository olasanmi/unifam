var ctxs = document.getElementById('myChartLesson');
if(ctxs) {
    $.ajax({
        url: '/get/lesson/count',
        method: 'GET',
        dataType: 'json',
        success: function (d) {
            var myChart = new Chart(ctxs, {
                type: 'line',
                data: {
                    labels: ['ene', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'ago','sep', 'oct', 'nov', 'dec'],
                    datasets: [{
                        label: 'Clases',
                        data: [d.enero, d.febrero, d.marzo, d.abril, d.mayo, d.junio, d.julio, d.agosto, d.septiembre, d.octubre, d.noviembre, d.diciembre],
                        borderWidth: 2
                    }]
                },
                options: {
                }
            }); 
        }
    });
}
