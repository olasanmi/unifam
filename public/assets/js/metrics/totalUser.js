var ctx = document.getElementById('myChart');
if(ctx) {
    var trainer
    var secretaria
    var client
    var admin

    $.ajax({
        url: '/get/users/count',
        method: 'GET',
        dataType: 'json',
        success: function (d) {
            console.log(d[0])
            trainer = parseInt(d[0].trainers)
            secretaria = parseInt(d[0].secretaria)
            client = parseInt(d[0].client)
            admin = parseInt(d[0].admin) 
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: ['Clientes', 'Entrenadores', 'Secretarias'],
                    datasets: [{
                        label: 'Total Usuarios',
                        data: [client, trainer, secretaria],
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 12, 19, 0.2)',
                        ],
                        borderColor: [
                            'rgba(255, 99, 132, 1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(75, 12, 19, 0.2)'
                        ],
                        borderWidth: 2
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            }); 
        }
    });
}
