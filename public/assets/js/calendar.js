document.addEventListener('DOMContentLoaded', function() {
  var calendarEl = document.getElementById('calendar');
  if(calendarEl) {
    var calendar = new FullCalendar.Calendar(calendarEl, {
    header: {
      left: 'prev,next, today',
      right: 'title',
    },
    locale: 'es',
    themeSystem: 'bootstrap',
    plugins: [ 'bootstrap', 'dayGrid' ],
    timeZone: 'UTC',
    defaultView: 'dayGridMonth',
    height: 350,
    events: {
      url: '/lessons/list/now'
    },
    eventRender: function(info) {
      if (info.event.extendedProps.status === 'finish') {
        var dotEl = info.el.getElementsByClassName('fc-content')[0];
        if (dotEl) {
          dotEl.style.backgroundColor = 'red';
          dotEl.style.borderColor = 'red';
        }
      } else if (info.event.extendedProps.status === 'coming') {
        var dotEl = info.el.getElementsByClassName('fc-content')[0];
        if (dotEl) {
          dotEl.style.backgroundColor = 'green';
          dotEl.style.borderColor = 'green';
        }
      } else if (info.event.extendedProps.status === 'today') {
        var dotEl = info.el.getElementsByClassName('fc-content')[0];
        if (dotEl) {
          dotEl.style.backgroundColor = 'green';
          dotEl.style.borderColor = 'green';
        }
      }
    }
  });
  calendar.render();
  }
});