$(function () {
  $('[data-toggle="popover"]').popover()
})



$("#payment-button").click(function(e) {
	$("#payment-button-amount").hide()
	$("#payment-button-sending").show()
    var form = $(this).parents('form');
    var cvv = $('#x_card_code').val();
    var regCVV = /^[0-9]{3,4}$/;
    var CardNo = $('#cc-number').val();
    var regCardNo = /^[0-9]{12,16}$/;
    var date = $('#cc-exp').val().split('/');
    var regMonth = /^01|02|03|04|05|06|07|08|09|10|11|12$/;
    var regYear = /^20|21|22|23|24|25|26|27|28|29|30|31$/;
    
    if (form[0].checkValidity() === false) {
      e.preventDefault();
      e.stopPropagation();
      $("#payment-button-amount").show()
	$("#payment-button-sending").hide()
    }
    else {
       if (!regCardNo.test(CardNo)) {
       
        $("#cc-number").addClass('required');
        $("#cc-number").focus();
        alert("Ingresa un número valido de tarjeta de  12 a 16 digitos!");
        $("#payment-button-amount").show()
		$("#payment-button-sending").hide()
        return false;
      }
      else if (!regCVV.test(cvv)) {
       
        $("#x_card_code").addClass('required');
        $("#x_card_code").focus();
        alert("Ingresa el código de 3 digitos detras de la tarjeta que sea valido!");
        $("#payment-button-amount").show()
		$("#payment-button-sending").hide()
        return false;
      }
      else if (!regMonth.test(date[0]) && !regMonth.test(date[1]) ) {
       
        $("#cc_exp").addClass('required');
        $("#cc_exp").focus();
        alert("Ingresa la fecha de expiracion");
        $("#payment-button-amount").show()
		$("#payment-button-sending").hide()
        return false;
      }

        var kushki = new Kushki({
		  merchantId: '20000000102051406000', 
		  inTestEnvironment: true,
		  regional:false
		});

		var callback = function(response) {
		if(!response.code){
		    document.getElementById("cc-token").value = response.token
		    form.submit()
		  } else {
		    console.error('Error: ',response.error, 'Code: ', response.code, 'Message: ',response.message);
		  }
		}

		var card = document.getElementById("cc-number").value
		var cvc = document.getElementById("x_card_code").value
		var date = document.getElementById("cc-exp").value
		var dateSplit = date.split('/')
		var customer = document.getElementById("cc-user").value
		document.getElementById("cc-mes").value = dateSplit[0]
		document.getElementById("cc-año").value = dateSplit[1]

		kushki.requestSubscriptionToken({
		  card: {
		    name: customer,
		    number: card,
		    cvc: cvc,
		    expiryMonth: dateSplit[0],
		    expiryYear: dateSplit[1]
		},
		  currency: "COP"
		}, callback);
		return false
    }
    form.addClass('was-validated');
});
