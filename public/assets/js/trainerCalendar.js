document.addEventListener('DOMContentLoaded', function() {
  var calendarEl = document.getElementById('calendar2');
  if(calendarEl) {
    var calendar = new FullCalendar.Calendar(calendarEl, {
    header: {
      left: 'prev,next, today',
      right: 'title',
    },
    locale: 'es',
    plugins: [ 'bootstrap', 'dayGrid' ],
    themeSystem: 'bootstrap',
    timeZone: 'UTC',
    defaultView: 'dayGridMonth',
    height: 450,
    events: {
      url: '/lessons/list/trainers'
    },
    eventRender: function(info) {
      if (info.event.extendedProps.status === 'finish') {
        var dotEl = info.el.getElementsByClassName('fc-day-grid-event')[0];
        if (dotEl) {
          dotEl.style.backgroundColor = 'red';
        }
      } else if (info.event.extendedProps.status === 'coming') {
        var dotEl = info.el.getElementsByClassName('fc-day-grid-event')[0];
        if (dotEl) {
          dotEl.style.backgroundColor = 'green';
        }
      } else if (info.event.extendedProps.status === 'today') {
        var dotEl = info.el.getElementsByClassName('fc-day-grid-event')[0];
        if (dotEl) {
          dotEl.style.backgroundColor = 'green';
        }
      }
    }
  });
  calendar.render();
  }
});