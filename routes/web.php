<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Model
//index route
Route::get('/', function () {
    return redirect('/login');
});

//auth rutes
Auth::routes(['verify' => true]);
//dashboard rutes
Route::group(['prefix' => 'dashboard', 'middleware' => 'auth' ], function () {
    
    Route::get('main', 'UsersController@main')->middleware('verified');
    Route::get('home', 'UsersController@main')->middleware('verified');
    
    Route::group(['prefix' => 'loans', 'middleware' => 'auth' ], function () {
        Route::get('/', 'LoanController@index');
        Route::get('/add', 'LoanController@create');
        Route::post('/store', 'LoanController@store');
    });
});
