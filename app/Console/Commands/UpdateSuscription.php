<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

//models
use App\Suscription;
use App\Facturing;
use App\Plan;
//auth
use Auth;
//kushki
use kushki\lib\Amount;
use kushki\lib\ExtraTaxes;
use kushki\lib\KushkiCurrency;
use kushki\lib\KushkiLanguage;
use kushki\lib\Kushki;
use kushki\lib\KushkiEnvironment;

class UpdateSuscription extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'suscription:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'billing suscription if period end';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //kushki option
        $merchantId = "20000000108200620000";
        $language = KushkiLanguage::ES;
        $currency = KushkiCurrency::COP;
        $environment = KushkiEnvironment::TESTING;

        $kushki = new Kushki($merchantId, $language, $currency, $environment);
        //select suscription
        $metadata = array("Key1"=>"value1", "Key2"=>"value2");
        $date = date('Y-m-d');
        //get suscriptions
        $suscription = Suscription::where('is_active', '=', 1)
        ->where('end', '<=', $date)
        ->get();
        //cicle
        foreach ($suscription as $key => $value) {
            if ($value->auto_billing == 1) {
                $plan = Plan::findOrFail($value->plan_id);
                if($value->end <= $date) {
                    $chargeSubscription = $kushki->chargeSubscription($value->hushki_id , $metadata);
                    if ($chargeSubscription->isSuccessful()) {
                        $value->end = date ( 'Y-m-j' ,strtotime ( '+30 day' , strtotime ( date('Y-m-j') )));
                        $value->is_active = 1;
                        $value->save();
                        $factura = new Facturing();
                        $factura->kushki_id = $value->hushki_id;
                        $factura->plan_name = $plan->name;
                        $factura->price = $plan->price;
                        $factura->start = $value->start;
                        $factura->end = $value->end;
                        $factura->user_id = $value->user_id;
                        $factura->save();
                    }
                }
            } else {
                if($value->end <= $date) {
                    //cancel
                    $curl = curl_init();
                    curl_setopt_array($curl, array(
                      CURLOPT_URL => "https://api-uat.kushkipagos.com/subscriptions/v1/card/".$value->hushki_id ,
                      CURLOPT_RETURNTRANSFER => true,
                      CURLOPT_ENCODING => "",
                      CURLOPT_MAXREDIRS => 10,
                      CURLOPT_TIMEOUT => 30,
                      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                      CURLOPT_CUSTOMREQUEST => "DELETE",
                      CURLOPT_HTTPHEADER => array(
                        "content-type: application/json",
                        "private-merchant-id: 20000000108200620000"
                      ),
                    ));

                    $response = curl_exec($curl);
                    $err = curl_error($curl);

                    curl_close($curl);

                    if ($err) {
                      $value->delete();
                    } else {
                        $value->delete();
                    }
                }
            }
        }
    }
}
