<?php

namespace App\Http\Resources\Lesson;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Helpers\Helper;

class LessonCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->transform(function ($models){
            return [
                'id'            => $models->id,
                'title'         => $models->description,
                'start'         => $models->date.'T'.$models->hour,
                'state'         => Helper::setState($models->date)
            ];
        });
    }
}
