<?php
namespace App\Helpers;
use Illuminate\Support\Str;
use File;
use App\LessonCupe;
use App\Lesson;
use App\User;

class Helper {

    //upload file
    public static function sethPath($file, $pathModel){
        $name = date('Ymd_His').'-'.$file->getClientOriginalName();
        $file->move('image/'.$pathModel, $name);
        return $name;
    }

    //delete file
    public static function deleteFile($file, $pathModel){
        $oldimage = public_path("image/{$pathModel}/{$file}");
        if (File::exists($oldimage)){ 
            unlink($oldimage);
        }
    }

    //set lesson state
    public static function setState($date, $hour) {
        $today = date('Y-m-d');
        $hourNow = date('H:i');
        if($date < $today and $hour <= $hourNow) {
            return 'finish';
        } else if($date > $today) {
            return 'coming';
        } else if($date == $today and $hour >= $hourNow) {
            return 'today';
        } else if ($date == $today and $hour < $hourNow) {
            return 'finish';
        }
    }
    
    //valido user in class
    public static function validateCupeForClasss($cupe, $user) {
        $userId = Array();
        if($user->Suscription and $user->Suscription->is_active) {
            foreach ($cupe->Lesson->LessonCupes as $key => $value) {
                if($value->user_id == $user->id) {
                    array_push($userId, $value);
                }
            }
            if(count($userId) >= 1) {
                return 1;
            } else {
                return 3;
            }
        } else {
            return 2;
        }
    }

    //valido Numero de clases del plan con la del usuario
    public static function validateUserClassInPlan($user) {
        $now = date('Y-m-d');
        $userCupes = Array();
        $lessons = Lesson::where('month', '=', substr($now, 5, 2))
        ->get();
        foreach ($lessons as $key => $lesson) {
            foreach ($lesson->LessonCupes as $key => $cupe) {
                if($cupe->user_id == $user->id) {
                    array_push($userCupes, $cupe);
                }
            }
        }

        if(count($userCupes) >= $user->Suscription->Plan->number_class) {
            return 1;
        }
    }

    //validate suscription end
    public static function validateFunctionEnd($user) {
        $now = date('Y-m-d');
        if($user->Suscription->end <= $now) {
            return 5;
        }
    }

}